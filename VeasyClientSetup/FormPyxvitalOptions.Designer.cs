﻿namespace VeasyClientSetup
{
    partial class formPyxvitalOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupPyxvital = new System.Windows.Forms.GroupBox();
            this.textActivationCode = new System.Windows.Forms.TextBox();
            this.labelActivationCode = new System.Windows.Forms.Label();
            this.textDataPath = new System.Windows.Forms.TextBox();
            this.labelDataPath = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupPyxvital.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPyxvital
            // 
            this.groupPyxvital.Controls.Add(this.textActivationCode);
            this.groupPyxvital.Controls.Add(this.labelActivationCode);
            this.groupPyxvital.Controls.Add(this.textDataPath);
            this.groupPyxvital.Controls.Add(this.labelDataPath);
            this.groupPyxvital.Location = new System.Drawing.Point(15, 15);
            this.groupPyxvital.Margin = new System.Windows.Forms.Padding(4);
            this.groupPyxvital.Name = "groupPyxvital";
            this.groupPyxvital.Padding = new System.Windows.Forms.Padding(4);
            this.groupPyxvital.Size = new System.Drawing.Size(472, 283);
            this.groupPyxvital.TabIndex = 2;
            this.groupPyxvital.TabStop = false;
            this.groupPyxvital.Text = "Options de Pyxvital";
            // 
            // textActivationCode
            // 
            this.textActivationCode.AcceptsReturn = true;
            this.textActivationCode.Location = new System.Drawing.Point(182, 62);
            this.textActivationCode.Margin = new System.Windows.Forms.Padding(4);
            this.textActivationCode.Multiline = true;
            this.textActivationCode.Name = "textActivationCode";
            this.textActivationCode.Size = new System.Drawing.Size(277, 211);
            this.textActivationCode.TabIndex = 1;
            // 
            // labelActivationCode
            // 
            this.labelActivationCode.AutoSize = true;
            this.labelActivationCode.Location = new System.Drawing.Point(37, 65);
            this.labelActivationCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelActivationCode.Name = "labelActivationCode";
            this.labelActivationCode.Size = new System.Drawing.Size(137, 20);
            this.labelActivationCode.TabIndex = 0;
            this.labelActivationCode.Text = "Code d\'activation";
            // 
            // textDataPath
            // 
            this.textDataPath.Location = new System.Drawing.Point(182, 31);
            this.textDataPath.Margin = new System.Windows.Forms.Padding(4);
            this.textDataPath.Name = "textDataPath";
            this.textDataPath.Size = new System.Drawing.Size(277, 27);
            this.textDataPath.TabIndex = 1;
            this.textDataPath.Text = "C:\\ProgramData\\Pyxvital\\";
            // 
            // labelDataPath
            // 
            this.labelDataPath.AutoSize = true;
            this.labelDataPath.Location = new System.Drawing.Point(8, 34);
            this.labelDataPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDataPath.Name = "labelDataPath";
            this.labelDataPath.Size = new System.Drawing.Size(166, 20);
            this.labelDataPath.TabIndex = 0;
            this.labelDataPath.Text = "Chemin des données";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(407, 315);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 29);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(309, 315);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(82, 29);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // formPyxvitalOptions
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(500, 359);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupPyxvital);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formPyxvitalOptions";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Veasy Client Setup";
            this.groupPyxvital.ResumeLayout(false);
            this.groupPyxvital.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupPyxvital;
        private System.Windows.Forms.TextBox textActivationCode;
        private System.Windows.Forms.Label labelActivationCode;
        private System.Windows.Forms.TextBox textDataPath;
        private System.Windows.Forms.Label labelDataPath;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
    }
}

