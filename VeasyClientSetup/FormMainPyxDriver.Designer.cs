﻿namespace VeasyClientSetup
{
    partial class formMainPyxDriver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListTools = new System.Windows.Forms.CheckedListBox();
            this.groupPyxvital = new System.Windows.Forms.GroupBox();
            this.textLog = new System.Windows.Forms.TextBox();
            this.radioPyxDriverCsd = new System.Windows.Forms.RadioButton();
            this.radioPyxDriverLib = new System.Windows.Forms.RadioButton();
            this.labelPyxvitalType = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonUninstall = new System.Windows.Forms.Button();
            this.groupPyxvital.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkedListTools
            // 
            this.checkedListTools.CheckOnClick = true;
            this.checkedListTools.FormattingEnabled = true;
            this.checkedListTools.Location = new System.Drawing.Point(13, 46);
            this.checkedListTools.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListTools.Name = "checkedListTools";
            this.checkedListTools.Size = new System.Drawing.Size(350, 202);
            this.checkedListTools.TabIndex = 1;
            // 
            // groupPyxvital
            // 
            this.groupPyxvital.Controls.Add(this.checkedListTools);
            this.groupPyxvital.Controls.Add(this.textLog);
            this.groupPyxvital.Controls.Add(this.radioPyxDriverCsd);
            this.groupPyxvital.Controls.Add(this.radioPyxDriverLib);
            this.groupPyxvital.Controls.Add(this.labelPyxvitalType);
            this.groupPyxvital.Location = new System.Drawing.Point(15, 15);
            this.groupPyxvital.Margin = new System.Windows.Forms.Padding(4);
            this.groupPyxvital.Name = "groupPyxvital";
            this.groupPyxvital.Padding = new System.Windows.Forms.Padding(4);
            this.groupPyxvital.Size = new System.Drawing.Size(379, 542);
            this.groupPyxvital.TabIndex = 2;
            this.groupPyxvital.TabStop = false;
            // 
            // textLog
            // 
            this.textLog.Location = new System.Drawing.Point(13, 261);
            this.textLog.Margin = new System.Windows.Forms.Padding(4);
            this.textLog.Multiline = true;
            this.textLog.Name = "textLog";
            this.textLog.ReadOnly = true;
            this.textLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textLog.Size = new System.Drawing.Size(350, 265);
            this.textLog.TabIndex = 5;
            // 
            // radioPyxDriverCsd
            // 
            this.radioPyxDriverCsd.AutoSize = true;
            this.radioPyxDriverCsd.Location = new System.Drawing.Point(200, 13);
            this.radioPyxDriverCsd.Name = "radioPyxDriverCsd";
            this.radioPyxDriverCsd.Size = new System.Drawing.Size(54, 21);
            this.radioPyxDriverCsd.TabIndex = 3;
            this.radioPyxDriverCsd.Text = "CSD";
            this.radioPyxDriverCsd.UseVisualStyleBackColor = true;
            // 
            // radioPyxDriverLib
            // 
            this.radioPyxDriverLib.AutoSize = true;
            this.radioPyxDriverLib.Checked = true;
            this.radioPyxDriverLib.Location = new System.Drawing.Point(129, 13);
            this.radioPyxDriverLib.Name = "radioPyxDriverLib";
            this.radioPyxDriverLib.Size = new System.Drawing.Size(46, 21);
            this.radioPyxDriverLib.TabIndex = 3;
            this.radioPyxDriverLib.TabStop = true;
            this.radioPyxDriverLib.Text = "LIB";
            this.radioPyxDriverLib.UseVisualStyleBackColor = true;
            // 
            // labelPyxvitalType
            // 
            this.labelPyxvitalType.AutoSize = true;
            this.labelPyxvitalType.Location = new System.Drawing.Point(10, 15);
            this.labelPyxvitalType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPyxvitalType.Name = "labelPyxvitalType";
            this.labelPyxvitalType.Size = new System.Drawing.Size(99, 17);
            this.labelPyxvitalType.TabIndex = 0;
            this.labelPyxvitalType.Text = "PyxDriver type";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(311, 580);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 29);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(221, 580);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(82, 29);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonUninstall
            // 
            this.buttonUninstall.Location = new System.Drawing.Point(130, 580);
            this.buttonUninstall.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUninstall.Name = "buttonUninstall";
            this.buttonUninstall.Size = new System.Drawing.Size(83, 29);
            this.buttonUninstall.TabIndex = 7;
            this.buttonUninstall.Text = "Uninstall";
            this.buttonUninstall.UseVisualStyleBackColor = true;
            this.buttonUninstall.Click += new System.EventHandler(this.buttonUninstall_Click);
            // 
            // formMainPyxDriver
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(405, 622);
            this.Controls.Add(this.buttonUninstall);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupPyxvital);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formMainPyxDriver";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Veasy Client Setup - PyxDriver";
            this.groupPyxvital.ResumeLayout(false);
            this.groupPyxvital.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListTools;
        private System.Windows.Forms.GroupBox groupPyxvital;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelPyxvitalType;
        private System.Windows.Forms.RadioButton radioPyxDriverLib;
        private System.Windows.Forms.RadioButton radioPyxDriverCsd;
        private System.Windows.Forms.TextBox textLog;
        private System.Windows.Forms.Button buttonUninstall;
    }
}

