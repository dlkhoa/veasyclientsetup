﻿namespace VeasyClientSetup
{
    partial class formMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListTools = new System.Windows.Forms.CheckedListBox();
            this.groupPyxvital = new System.Windows.Forms.GroupBox();
            this.textLog = new System.Windows.Forms.TextBox();
            this.checkStartPyxvital = new System.Windows.Forms.CheckBox();
            this.textActivationCode = new System.Windows.Forms.TextBox();
            this.labelStartPyxvital = new System.Windows.Forms.Label();
            this.labelActivationCode = new System.Windows.Forms.Label();
            this.textDataPath = new System.Windows.Forms.TextBox();
            this.labelDataPath = new System.Windows.Forms.Label();
            this.radioPyxDriverCsd = new System.Windows.Forms.RadioButton();
            this.radioPyxDriverLib = new System.Windows.Forms.RadioButton();
            this.labelPyxvitalType = new System.Windows.Forms.Label();
            this.groupTools = new System.Windows.Forms.GroupBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonUninstall = new System.Windows.Forms.Button();
            this.groupPyxvital.SuspendLayout();
            this.groupTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkedListTools
            // 
            this.checkedListTools.CheckOnClick = true;
            this.checkedListTools.FormattingEnabled = true;
            this.checkedListTools.Location = new System.Drawing.Point(12, 31);
            this.checkedListTools.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListTools.Name = "checkedListTools";
            this.checkedListTools.Size = new System.Drawing.Size(261, 268);
            this.checkedListTools.TabIndex = 1;
            // 
            // groupPyxvital
            // 
            this.groupPyxvital.Controls.Add(this.textLog);
            this.groupPyxvital.Controls.Add(this.checkStartPyxvital);
            this.groupPyxvital.Controls.Add(this.textActivationCode);
            this.groupPyxvital.Controls.Add(this.labelStartPyxvital);
            this.groupPyxvital.Controls.Add(this.labelActivationCode);
            this.groupPyxvital.Controls.Add(this.textDataPath);
            this.groupPyxvital.Controls.Add(this.labelDataPath);
            this.groupPyxvital.Location = new System.Drawing.Point(15, 15);
            this.groupPyxvital.Margin = new System.Windows.Forms.Padding(4);
            this.groupPyxvital.Name = "groupPyxvital";
            this.groupPyxvital.Padding = new System.Windows.Forms.Padding(4);
            this.groupPyxvital.Size = new System.Drawing.Size(472, 534);
            this.groupPyxvital.TabIndex = 2;
            this.groupPyxvital.TabStop = false;
            this.groupPyxvital.Text = "Pyxvital options";
            // 
            // textLog
            // 
            this.textLog.Location = new System.Drawing.Point(8, 305);
            this.textLog.Margin = new System.Windows.Forms.Padding(4);
            this.textLog.Multiline = true;
            this.textLog.Name = "textLog";
            this.textLog.ReadOnly = true;
            this.textLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textLog.Size = new System.Drawing.Size(451, 221);
            this.textLog.TabIndex = 5;
            // 
            // checkStartPyxvital
            // 
            this.checkStartPyxvital.AutoSize = true;
            this.checkStartPyxvital.Checked = true;
            this.checkStartPyxvital.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkStartPyxvital.Location = new System.Drawing.Point(148, 284);
            this.checkStartPyxvital.Name = "checkStartPyxvital";
            this.checkStartPyxvital.Size = new System.Drawing.Size(18, 17);
            this.checkStartPyxvital.TabIndex = 2;
            this.checkStartPyxvital.UseVisualStyleBackColor = true;
            // 
            // textActivationCode
            // 
            this.textActivationCode.AcceptsReturn = true;
            this.textActivationCode.Location = new System.Drawing.Point(148, 62);
            this.textActivationCode.Margin = new System.Windows.Forms.Padding(4);
            this.textActivationCode.Multiline = true;
            this.textActivationCode.Name = "textActivationCode";
            this.textActivationCode.Size = new System.Drawing.Size(311, 211);
            this.textActivationCode.TabIndex = 1;
            // 
            // labelStartPyxvital
            // 
            this.labelStartPyxvital.AutoSize = true;
            this.labelStartPyxvital.Location = new System.Drawing.Point(51, 282);
            this.labelStartPyxvital.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelStartPyxvital.Name = "labelStartPyxvital";
            this.labelStartPyxvital.Size = new System.Drawing.Size(107, 20);
            this.labelStartPyxvital.TabIndex = 0;
            this.labelStartPyxvital.Text = "Start Pyxvital";
            // 
            // labelActivationCode
            // 
            this.labelActivationCode.AutoSize = true;
            this.labelActivationCode.Location = new System.Drawing.Point(36, 65);
            this.labelActivationCode.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelActivationCode.Name = "labelActivationCode";
            this.labelActivationCode.Size = new System.Drawing.Size(123, 20);
            this.labelActivationCode.TabIndex = 0;
            this.labelActivationCode.Text = "Activation code";
            // 
            // textDataPath
            // 
            this.textDataPath.Location = new System.Drawing.Point(148, 31);
            this.textDataPath.Margin = new System.Windows.Forms.Padding(4);
            this.textDataPath.Name = "textDataPath";
            this.textDataPath.Size = new System.Drawing.Size(311, 27);
            this.textDataPath.TabIndex = 1;
            this.textDataPath.Text = "C:\\ProgramData\\Pyxvital\\";
            // 
            // labelDataPath
            // 
            this.labelDataPath.AutoSize = true;
            this.labelDataPath.Location = new System.Drawing.Point(70, 31);
            this.labelDataPath.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelDataPath.Name = "labelDataPath";
            this.labelDataPath.Size = new System.Drawing.Size(82, 20);
            this.labelDataPath.TabIndex = 0;
            this.labelDataPath.Text = "Data path";
            // 
            // radioPyxDriverCsd
            // 
            this.radioPyxDriverCsd.AutoSize = true;
            this.radioPyxDriverCsd.Location = new System.Drawing.Point(694, 337);
            this.radioPyxDriverCsd.Name = "radioPyxDriverCsd";
            this.radioPyxDriverCsd.Size = new System.Drawing.Size(66, 24);
            this.radioPyxDriverCsd.TabIndex = 3;
            this.radioPyxDriverCsd.Text = "CSD";
            this.radioPyxDriverCsd.UseVisualStyleBackColor = true;
            this.radioPyxDriverCsd.CheckedChanged += new System.EventHandler(this.radioPyxvitalDriver_CheckedChanged);
            // 
            // radioPyxDriverLib
            // 
            this.radioPyxDriverLib.AutoSize = true;
            this.radioPyxDriverLib.Checked = true;
            this.radioPyxDriverLib.Location = new System.Drawing.Point(623, 337);
            this.radioPyxDriverLib.Name = "radioPyxDriverLib";
            this.radioPyxDriverLib.Size = new System.Drawing.Size(56, 24);
            this.radioPyxDriverLib.TabIndex = 3;
            this.radioPyxDriverLib.TabStop = true;
            this.radioPyxDriverLib.Text = "LIB";
            this.radioPyxDriverLib.UseVisualStyleBackColor = true;
            this.radioPyxDriverLib.CheckedChanged += new System.EventHandler(this.radioPyxvitalClassic_CheckedChanged);
            // 
            // labelPyxvitalType
            // 
            this.labelPyxvitalType.AutoSize = true;
            this.labelPyxvitalType.Location = new System.Drawing.Point(504, 339);
            this.labelPyxvitalType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPyxvitalType.Name = "labelPyxvitalType";
            this.labelPyxvitalType.Size = new System.Drawing.Size(118, 20);
            this.labelPyxvitalType.TabIndex = 0;
            this.labelPyxvitalType.Text = "PyxDriver type";
            // 
            // groupTools
            // 
            this.groupTools.Controls.Add(this.checkedListTools);
            this.groupTools.Location = new System.Drawing.Point(505, 15);
            this.groupTools.Margin = new System.Windows.Forms.Padding(4);
            this.groupTools.Name = "groupTools";
            this.groupTools.Padding = new System.Windows.Forms.Padding(4);
            this.groupTools.Size = new System.Drawing.Size(283, 315);
            this.groupTools.TabIndex = 3;
            this.groupTools.TabStop = false;
            this.groupTools.Text = "Tools";
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(708, 520);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 29);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(618, 520);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(82, 29);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonUninstall
            // 
            this.buttonUninstall.Location = new System.Drawing.Point(527, 520);
            this.buttonUninstall.Margin = new System.Windows.Forms.Padding(4);
            this.buttonUninstall.Name = "buttonUninstall";
            this.buttonUninstall.Size = new System.Drawing.Size(83, 29);
            this.buttonUninstall.TabIndex = 7;
            this.buttonUninstall.Text = "Uninstall";
            this.buttonUninstall.UseVisualStyleBackColor = true;
            this.buttonUninstall.Click += new System.EventHandler(this.buttonUninstall_Click);
            // 
            // formMain
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(802, 562);
            this.Controls.Add(this.buttonUninstall);
            this.Controls.Add(this.radioPyxDriverCsd);
            this.Controls.Add(this.radioPyxDriverLib);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.labelPyxvitalType);
            this.Controls.Add(this.groupTools);
            this.Controls.Add(this.groupPyxvital);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Veasy Client Setup";
            this.groupPyxvital.ResumeLayout(false);
            this.groupPyxvital.PerformLayout();
            this.groupTools.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListTools;
        private System.Windows.Forms.GroupBox groupPyxvital;
        private System.Windows.Forms.TextBox textActivationCode;
        private System.Windows.Forms.Label labelActivationCode;
        private System.Windows.Forms.TextBox textDataPath;
        private System.Windows.Forms.Label labelDataPath;
        private System.Windows.Forms.GroupBox groupTools;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.CheckBox checkStartPyxvital;
        private System.Windows.Forms.Label labelStartPyxvital;
        private System.Windows.Forms.Label labelPyxvitalType;
        private System.Windows.Forms.RadioButton radioPyxDriverLib;
        private System.Windows.Forms.RadioButton radioPyxDriverCsd;
        private System.Windows.Forms.TextBox textLog;
        private System.Windows.Forms.Button buttonUninstall;
    }
}

