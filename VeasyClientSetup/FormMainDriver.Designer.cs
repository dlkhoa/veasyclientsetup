﻿namespace VeasyClientSetup
{
    partial class formMainDriver
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.checkedListTools = new System.Windows.Forms.CheckedListBox();
            this.radioPyxDriverCsd = new System.Windows.Forms.RadioButton();
            this.radioPyxDriverLib = new System.Windows.Forms.RadioButton();
            this.labelPyxvitalType = new System.Windows.Forms.Label();
            this.groupTools = new System.Windows.Forms.GroupBox();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupTools.SuspendLayout();
            this.SuspendLayout();
            // 
            // checkedListTools
            // 
            this.checkedListTools.CheckOnClick = true;
            this.checkedListTools.FormattingEnabled = true;
            this.checkedListTools.Location = new System.Drawing.Point(12, 67);
            this.checkedListTools.Margin = new System.Windows.Forms.Padding(4);
            this.checkedListTools.Name = "checkedListTools";
            this.checkedListTools.Size = new System.Drawing.Size(261, 382);
            this.checkedListTools.TabIndex = 1;
            // 
            // radioPyxDriverCsd
            // 
            this.radioPyxDriverCsd.AutoSize = true;
            this.radioPyxDriverCsd.Location = new System.Drawing.Point(203, 23);
            this.radioPyxDriverCsd.Name = "radioPyxDriverCsd";
            this.radioPyxDriverCsd.Size = new System.Drawing.Size(54, 21);
            this.radioPyxDriverCsd.TabIndex = 3;
            this.radioPyxDriverCsd.Text = "CSD";
            this.radioPyxDriverCsd.UseVisualStyleBackColor = true;
            // 
            // radioPyxDriverLib
            // 
            this.radioPyxDriverLib.AutoSize = true;
            this.radioPyxDriverLib.Checked = true;
            this.radioPyxDriverLib.Location = new System.Drawing.Point(132, 23);
            this.radioPyxDriverLib.Name = "radioPyxDriverLib";
            this.radioPyxDriverLib.Size = new System.Drawing.Size(46, 21);
            this.radioPyxDriverLib.TabIndex = 3;
            this.radioPyxDriverLib.TabStop = true;
            this.radioPyxDriverLib.Text = "LIB";
            this.radioPyxDriverLib.UseVisualStyleBackColor = true;
            // 
            // labelPyxvitalType
            // 
            this.labelPyxvitalType.AutoSize = true;
            this.labelPyxvitalType.Location = new System.Drawing.Point(13, 25);
            this.labelPyxvitalType.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelPyxvitalType.Name = "labelPyxvitalType";
            this.labelPyxvitalType.Size = new System.Drawing.Size(99, 17);
            this.labelPyxvitalType.TabIndex = 0;
            this.labelPyxvitalType.Text = "PyxDriver type";
            // 
            // groupTools
            // 
            this.groupTools.Controls.Add(this.radioPyxDriverCsd);
            this.groupTools.Controls.Add(this.radioPyxDriverLib);
            this.groupTools.Controls.Add(this.checkedListTools);
            this.groupTools.Controls.Add(this.labelPyxvitalType);
            this.groupTools.Location = new System.Drawing.Point(13, 13);
            this.groupTools.Margin = new System.Windows.Forms.Padding(4);
            this.groupTools.Name = "groupTools";
            this.groupTools.Padding = new System.Windows.Forms.Padding(4);
            this.groupTools.Size = new System.Drawing.Size(283, 477);
            this.groupTools.TabIndex = 3;
            this.groupTools.TabStop = false;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(216, 518);
            this.buttonCancel.Margin = new System.Windows.Forms.Padding(4);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(80, 29);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(126, 518);
            this.buttonOK.Margin = new System.Windows.Forms.Padding(4);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(82, 29);
            this.buttonOK.TabIndex = 6;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // formMainDriver
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(308, 562);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupTools);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "formMainDriver";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Veasy Client Setup";
            this.groupTools.ResumeLayout(false);
            this.groupTools.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox checkedListTools;
        private System.Windows.Forms.GroupBox groupTools;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Label labelPyxvitalType;
        private System.Windows.Forms.RadioButton radioPyxDriverLib;
        private System.Windows.Forms.RadioButton radioPyxDriverCsd;
    }
}

