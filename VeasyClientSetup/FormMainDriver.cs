﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace VeasyClientSetup
{
    public partial class formMainDriver : Form
    {
        private IList<VeasyClientTool> clientTools;
        private int logTopClassic = 0;
        private int logTopDriver = 0;
        private int logBottom = 0;
        private Timer timerUninstall = new Timer();

        public formMainDriver()
        {
            InitializeComponent();

            LoadToolList();
        }

        private void LoadToolList()
        {
            var i = 1;
            clientTools = new List<VeasyClientTool>();
            while (true)
            {
                var tool = ConfigurationManager.AppSettings[$"vcs:Tool{i}"];
                if (string.IsNullOrEmpty(tool)) break;
                clientTools.Add(new VeasyClientTool(tool));
                i++;
            }

            foreach (var vct in clientTools)
            {
                var isChecked = true;
                var isAdded = true;
                if (!string.IsNullOrEmpty(vct.OSType))
                {
                    if (Environment.Is64BitOperatingSystem)
                    {
                        isChecked = (vct.OSType == Constants.OS64);
                    }
                    else
                    {
                        isChecked = (vct.OSType == Constants.OS32);
                    }
                    if (!Environment.Is64BitOperatingSystem)
                    {
                        isAdded = (vct.OSType == Constants.OS32);
                    }
                }
                if (isAdded)
                {
                    checkedListTools.Items.Add(vct, isChecked);
                }
            }
        }

        private void SetupPyxvital()
        {
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var certificatePath = ConfigurationManager.AppSettings[$"vcs:CertificatePath"];
            var pyxvital = ConfigurationManager.AppSettings[$"vcs:Pyxvital"];
            var elements = pyxvital.Split(';');
            var pyxvitalPath = elements[0];
            var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, pyxvitalPath);
            var destinationPath = @"C:\ProgramData\";
            if (elements.Length > 1 && !string.IsNullOrEmpty(elements[1]))
            {
                destinationPath = elements[1];
            }
            destinationPath = Path.Combine(destinationPath, pyxvitalPath);
            if (radioPyxDriverLib.Checked)
            {
                var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, "PyxDriver_LIB.msi");
                Utils.Execute(toolPath);
            }
            else if (radioPyxDriverCsd.Checked)
            {
                var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, "PyxDriver_CSD.msi");
                Utils.Execute(toolPath);
            }
            // 4 - Create shortcuts
            destinationPath = "c:\\PyxDriver";
            var pyxcomserveurPath = Path.Combine(destinationPath, "pyxcomserveur.bat");
            var pyxvitalexePath = Path.Combine(destinationPath, "Pyxvital.exe");
            Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath);
            Utils.CreateShortcut("Pyxvital", pyxvitalexePath);
            Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath, "Startup");
            Utils.CreateShortcut("Pyxvital", pyxvitalexePath, "Startup");

            // 5 - Install certificates
            StopPyxvital();
            Utils.InstallCertificate(Path.Combine(destinationPath, "rootS.der"));
            Utils.InstallCertificate(Path.Combine(destinationPath, "server.der"));
            //var batPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, certificatePath, "rootS.bat");
            //Utils.Execute(batPath);

            // 6 - Start Pyxvital tools
            Utils.Execute(pyxcomserveurPath, workingDir: destinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
            Utils.Execute(pyxvitalexePath, workingDir: destinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
        }

        private void SetupVDImaging()
        {
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var vdImaging = ConfigurationManager.AppSettings[$"vcs:VDImaging"];
            var vdImagingPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, vdImaging);
            Utils.Execute(vdImagingPath);
        }

        private void SetupTools()
        {
            foreach (var item in checkedListTools.CheckedItems)
            {
                var tool = item as VeasyClientTool;
                if (tool != null)
                {
                    var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
                    var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, tool.FileName);
                    Utils.Execute(toolPath);
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            SetupPyxvital();
            SetupTools();
            SetupVDImaging();
            MessageBox.Show("Done installing components!");
            this.Close();
        }

        private void StopPyxvital()
        {
            var uninstallCmd = "taskkill";
            var uninstallArgs = "/IM Pyxvital.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            uninstallArgs = "/IM Pyxcomserveur.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
        }
    }
}
