@echo off
CD %~dp0
taskkill /IM Pyxvital.exe /F
taskkill /IM Pyxcomserveur.exe /F

copy /y server.pem %1
copy /y rootS.der %1
copy /y server.der %1

certutil.exe  -addstore -f "root" rootS.der
certutil.exe  -addstore -f "root" server.der
