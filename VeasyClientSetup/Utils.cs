﻿using IWshRuntimeLibrary;
using System;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;

namespace VeasyClientSetup
{
    class Utils
    {
        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                try
                {
                    file.CopyTo(temppath, true);
                }
                catch { }
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        public static void CreateShortcut(string name, string path, string folder = "Desktop", string iconLocation = "")
        {
            try
            {
                object shFolder = (object)folder;
                WshShell shell = new WshShell();
                string shortcutAddress = Path.Combine((string)shell.SpecialFolders.Item(ref shFolder), $"{name}.lnk");
                IWshShortcut shortcut = (IWshShortcut)shell.CreateShortcut(shortcutAddress);
                shortcut.Description = name;
                shortcut.TargetPath = path;
                shortcut.WorkingDirectory = Path.GetDirectoryName(path);
                shortcut.WindowStyle = 7; // 3 - Maximized; 7 = Minimized; 4 = Normal
                if (!string.IsNullOrEmpty(iconLocation))
                {
                    shortcut.IconLocation = iconLocation;
                }
                shortcut.Save();
            }
            catch { }
        }

        public static void InstallCertificate(string cerFileName)
        {
            /*X509Certificate2 certificate = new X509Certificate2(cerFileName);
            X509Store store = new X509Store(StoreName.TrustedPublisher, StoreLocation.LocalMachine);

            store.Open(OpenFlags.ReadWrite);
            store.Add(certificate);
            store.Close();*/
            /*string cmd = $"CertUtil";
            string arg = $"-UI {cerFileName} import";
            Execute(cmd, arg);*/
            Execute(cerFileName);
        }

        public static void Execute(string path, string workingDir = "", string arguments = "", bool waitForExit = true, ProcessWindowStyle windowStyle = ProcessWindowStyle.Normal)
        {
            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = path;
            if (!string.IsNullOrEmpty(arguments))
            {
                startInfo.Arguments = arguments;
            }
            if (!string.IsNullOrEmpty(workingDir))
            {
                startInfo.WorkingDirectory = workingDir;
            }
            startInfo.WindowStyle = windowStyle;

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    if (waitForExit)
                    {
                        exeProcess.WaitForExit();
                    }
                }
            }
            catch (Exception e)
            {
                // Log error.
            }
        }

        public static void DeleteDirectory(string dirPath, bool includeSubDirs = true)
        {
            // Get the subdirectories for the specified directory.
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(dirPath);
            // Delete this dir and all subdirs.
            try
            {
                di.Delete(includeSubDirs);
                Directory.Delete(dirPath);
            }
            catch (System.IO.IOException e)
            {
                //MessageBox.Show(e.Message);
            }
        }

        public static void DeleteShortcut(string name, string path, string folder = "Desktop")
        {
            object shFolder = (object)folder;
            WshShell shell = new WshShell();
            string shortcutPath = Path.Combine((string)shell.SpecialFolders.Item(ref shFolder), $"{name}.lnk");
            if (System.IO.File.Exists(shortcutPath))
            {
                System.IO.File.Delete(shortcutPath);
            }
        }

        public static string GetProductSearchWhere(string name)
        {
            var words = name.Split(' ');
            var where = string.Empty;
            foreach (var w in words)
            {
                if (!string.IsNullOrEmpty(where)) where += " and ";
                where += $"name like '%%{w}%%'";
            }
            return where;
        }

    }
}

