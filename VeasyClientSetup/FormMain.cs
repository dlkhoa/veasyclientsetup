﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace VeasyClientSetup
{
    public partial class formMain : Form
    {
        private IList<VeasyClientTool> clientTools;
        private int logTopClassic = 0;
        private int logTopDriver = 0;
        private int logBottom = 0;
        private Timer timerUninstall = new Timer();

        public formMain()
        {
            InitializeComponent();

            LoadToolList();

            logTopClassic = textLog.Top;
            logBottom = textLog.Bottom;
            logTopDriver = textDataPath.Top;
#if _PYXDRIVER || _PYXDRIVERCSD
            labelPyxvitalType.Visible = true;
            radioPyxDriverLib.Visible = true;
            radioPyxDriverCsd.Visible = true;
            radioPyxDriverLib.Checked = true;
            radioPyxDriverCsd.Checked = false;
#else
            labelPyxvitalType.Visible = false;
            radioPyxDriverLib.Visible = false;
            radioPyxDriverCsd.Visible = false;
#endif
            ArrangeControls();

#if _UNINSTALL
            timerUninstall.Interval = 1000; // 1 second
            timerUninstall.Tick += new EventHandler(timerUninstall_Tick);
            timerUninstall.Start();
#endif
        }

        private void LoadToolList()
        {
            var i = 1;
            clientTools = new List<VeasyClientTool>();
            while (true)
            {
                var tool = ConfigurationManager.AppSettings[$"vcs:Tool{i}"];
                if (string.IsNullOrEmpty(tool)) break;
                clientTools.Add(new VeasyClientTool(tool));
                i++;
            }

            foreach (var vct in clientTools)
            {
                var isChecked = true;
                var isAdded = true;
                if (!string.IsNullOrEmpty(vct.OSType))
                {
                    if (Environment.Is64BitOperatingSystem)
                    {
                        isChecked = (vct.OSType == Constants.OS64);
                    }
                    else
                    {
                        isChecked = (vct.OSType == Constants.OS32);
                    }
                    if (!Environment.Is64BitOperatingSystem)
                    {
                        isAdded = (vct.OSType == Constants.OS32);
                    }
                }
                if (isAdded)
                {
                    checkedListTools.Items.Add(vct, isChecked);
                }
            }
        }

        private void LogAction(string action, bool newLine = false)
        {
            textLog.AppendText(action);
            if (newLine)
            {
                textLog.AppendText("\r\n");
                textLog.Focus();
            }
        }

        private void SetupPyxvital()
        {
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var certificatePath = ConfigurationManager.AppSettings[$"vcs:CertificatePath"];
            var pyxvital = ConfigurationManager.AppSettings[$"vcs:Pyxvital"];
            var elements = pyxvital.Split(';');
            var pyxvitalPath = elements[0];
            var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, pyxvitalPath);
            var destinationPath = @"C:\ProgramData\";
            if (elements.Length > 1 && !string.IsNullOrEmpty(elements[1]))
            {
                destinationPath = elements[1];
            }
            destinationPath = Path.Combine(destinationPath, pyxvitalPath);
#if _PYXDRIVER
            if (radioPyxDriverLib.Checked)
            {
                LogAction($"Install PyxDriverLIB ... ");
                var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, "PyxDriver_LIB.msi");
                Utils.Execute(toolPath);
            }
            else if (radioPyxDriverCsd.Checked)
            {
                LogAction($"Install PyxDriverCSD ... ");
                var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, "PyxDriver_CSD.msi");
                Utils.Execute(toolPath);
            }
            LogAction($"[Done]", true);
#else
            // 1 - Copy Pyvital folder
            LogAction($"Copy Pyxvital from {sourcePath} to {destinationPath} ... ");
            Utils.DirectoryCopy(sourcePath, destinationPath, true);
            LogAction($"[Done]", true);

            // 2 - Update INI file
            LogAction($"Update Pyxvital INI file ... ");
            var pyxvitalDataPath = textDataPath.Text.Trim();
            if (!string.IsNullOrEmpty(pyxvitalDataPath))
            {
                var pyxvitalIni = new IniFile(Path.Combine(destinationPath, "Pyxvital.ini"));
                pyxvitalIni.Write("Fse", Path.Combine(pyxvitalDataPath, Path.Combine("FSE", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "FSE"));
                pyxvitalIni.Write("Lots", Path.Combine(pyxvitalDataPath, Path.Combine("LOTS", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "LOTS"));
                pyxvitalIni.Write("Fichiers", Path.Combine(pyxvitalDataPath, Path.Combine("B2", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "B2"));
                pyxvitalIni.Write("Arl", Path.Combine(pyxvitalDataPath, Path.Combine("ARL", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "ARL"));
                pyxvitalIni.Write("PJ", Path.Combine(pyxvitalDataPath, Path.Combine("PJ", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "PJ"));
                pyxvitalIni.Write("PS", Path.Combine(pyxvitalDataPath, "PS"), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "PS"));
                pyxvitalIni.Write("CV", Path.Combine(pyxvitalDataPath, "CV"), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "CV"));
            }
            LogAction($"[Done]", true);

            // 3 - Update Activation Code file
            LogAction($"Update Pyxvital Activation Code file ... ");
            try
            {
                var codeFilePath = Path.Combine(destinationPath, "CodPS.par");
                var append = File.Exists(codeFilePath);
                using (StreamWriter file = new StreamWriter(codeFilePath, append))
                {
                    if (!append)
                    {
                        file.WriteLine("[PS]");
                    }
                    else
                    {
                        file.WriteLine("");
                    }
                    file.WriteLine(textActivationCode.Text);
                }
            }
            catch { }
            LogAction($"[Done]", true);
#endif
            // 4 - Create shortcuts
#if _PYXDRIVER
            destinationPath = "c:\\PyxDriver";
#endif
            LogAction($"Create Pyxvital shortcuts ... ");
            var pyxcomserveurPath = Path.Combine(destinationPath, "pyxcomserveur.bat");
            var pyxvitalexePath = Path.Combine(destinationPath, "Pyxvital.exe");
            Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath);
            Utils.CreateShortcut("Pyxvital", pyxvitalexePath);
            Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath, "AllUsersStartup");
            Utils.CreateShortcut("Pyxvital", pyxvitalexePath, "AllUsersStartup");
            LogAction($"[Done]", true);

            // 5 - Install certificates
            LogAction($"Install Pyxvital certificates ... ");
            StopPyxvital();
            Utils.InstallCertificate(Path.Combine(destinationPath, "rootS.der"));
            Utils.InstallCertificate(Path.Combine(destinationPath, "server.der"));
            //var batPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, certificatePath, "rootS.bat");
            //Utils.Execute(batPath);
            LogAction($"[Done]", true);

            // 6 - Start Pyxvital tools
            var startPyxvital = false;
#if _PYXDRIVER
            startPyxvital = true;
#else
            startPyxvital = checkStartPyxvital.Checked;
#endif
            if (startPyxvital)
            {
                LogAction($"Start Pyxvital servers ... ");
                Utils.Execute(pyxcomserveurPath, workingDir: destinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
                Utils.Execute(pyxvitalexePath, workingDir: destinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
                LogAction($"[Done]", true);
            }
        }

        private void SetupVDImaging()
        {
            LogAction($"Install VDImaging ... ");
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var vdImaging = ConfigurationManager.AppSettings[$"vcs:VDImaging"];
            var vdImagingPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, vdImaging);
            Utils.Execute(vdImagingPath);
            LogAction($"[Done]", true);
        }

        private void SetupTools()
        {
            foreach (var item in checkedListTools.CheckedItems)
            {
                var tool = item as VeasyClientTool;
                if (tool != null)
                {
                    LogAction($"Install {tool.Name} ... ");
                    var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
                    var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, tool.FileName);
                    Utils.Execute(toolPath);
                    LogAction($"[Done]", true);
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            SetupPyxvital();
            SetupTools();
            SetupVDImaging();
            MessageBox.Show("Done installing components!");
            this.Close();
        }

        private void radioPyxvitalClassic_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePyxvitalControls();
        }

        private void radioPyxvitalDriver_CheckedChanged(object sender, EventArgs e)
        {
            UpdatePyxvitalControls();
        }

        private void UpdatePyxvitalControls()
        {
            //textDataPath.ReadOnly = radioPyxDriverCsd.Checked;
            //textActivationCode.ReadOnly = radioPyxDriverCsd.Checked;
        }

        private void ArrangeControls()
        {
#if _PYXDRIVER
            labelDataPath.Visible = false;
            textDataPath.Visible = false;
            labelActivationCode.Visible = false;
            textActivationCode.Visible = false;
            labelStartPyxvital.Visible = false;
            checkStartPyxvital.Visible = false;
            textLog.Top = logTopDriver;
#else
            labelDataPath.Visible = true;
            textDataPath.Visible = true;
            labelActivationCode.Visible = true;
            textActivationCode.Visible = true;
            labelStartPyxvital.Visible = true;
            checkStartPyxvital.Visible = true;
            textLog.Top = logTopClassic;
#endif
            textLog.Height = logBottom - textLog.Top;
#if _UNINSTALL
            this.Width = 0;
            this.Height = 0;
#endif
        }

        private void buttonUninstall_Click(object sender, EventArgs e)
        {
            Uninstall();
        }

        private void timerUninstall_Tick(object sender, EventArgs e)
        {
            timerUninstall.Stop();
            Uninstall();
        }

        private void Uninstall()
        {
            UninstallPyxvital();
            UninstallTools();
            //UninstallVDImaging();
            MessageBox.Show("Done uninstalling components!");
            this.Close();
        }

        private void UninstallPyxvital()
        {
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var certificatePath = ConfigurationManager.AppSettings[$"vcs:CertificatePath"];
            var pyxvital = ConfigurationManager.AppSettings[$"vcs:Pyxvital"];
            var elements = pyxvital.Split(';');
            var pyxvitalPath = elements[0];
            var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, pyxvitalPath);
            var destinationPath = @"C:\ProgramData\";
            if (elements.Length > 1 && !string.IsNullOrEmpty(elements[1]))
            {
                destinationPath = elements[1];
            }
            destinationPath = Path.Combine(destinationPath, pyxvitalPath);
            StopPyxvital();
            if (radioPyxDriverLib.Checked)
            {
                // 1 - Delete Pyvital folder
                LogAction($"Delete Pyxvital from {destinationPath} ... ");
                Utils.DeleteDirectory(destinationPath);
                LogAction($"[Done]", true);

            }
            else if (radioPyxDriverCsd.Checked)
            {
                LogAction($"Uninstall PyxDriverLIB ... ");
                var uninstallCmd = "wmic";
                var uninstallArgs = "product where \"name like 'PyxDriver%%'\" call uninstall";
                Utils.Execute(uninstallCmd, arguments: uninstallArgs);
                LogAction($"[Done]", true);
            }
            //4 - Delete shortcuts
            if (radioPyxDriverCsd.Checked)
            {
                destinationPath = "c:\\PyxDriver";
            }
            LogAction($"Delete Pyxvital shortcuts ... ");
            var pyxcomserveurPath = Path.Combine(destinationPath, "pyxcomserveur.bat");
            var pyxvitalexePath = Path.Combine(destinationPath, "Pyxvital.exe");
            Utils.DeleteShortcut("PyxcomServeur", pyxcomserveurPath);
            Utils.DeleteShortcut("Pyxvital", pyxvitalexePath);
            Utils.DeleteShortcut("PyxcomServeur", pyxcomserveurPath, "AllUsersStartup");
            Utils.DeleteShortcut("Pyxvital", pyxvitalexePath, "AllUsersStartup");
            LogAction($"[Done]", true);

        }

        private void UninstallTools()
        {
            foreach (var item in checkedListTools.CheckedItems)
            {
                var tool = item as VeasyClientTool;
                if (tool != null)
                {
                    LogAction($"Uninstall {tool.Name} ... ");
                    var toolName = tool.Name;
                    // Uninstall JRE may uninstall other JAVAs
                    /*if (toolName == "Jre32")
                    {
                        toolName = "Java%%Update%%32";
                    }
                    if (toolName == "Jre64")
                    {
                        toolName = "Java%%Update%%64";
                    }*/
                    toolName = $"%%{toolName}%%";
                    var uninstallCmd = $"wmic";
                    //var uninstallArgs = $"product where \"{Utils.GetProductSearchWhere(tool.Name)}\" call uninstall";
                    var uninstallArgs = $"product where \"name like '{toolName}'\" call uninstall";
                    Utils.Execute(uninstallCmd, arguments: uninstallArgs);
                    LogAction($"[Done]", true);
                }
            }
        }

        private void UninstallVDImaging()
        {
            LogAction($"Uninstall VDImaging ... ");
            var uninstallCmd = $"wmic";
            var uninstallArgs = $"product where \"name like 'VDImaging%%'\" call uninstall";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            LogAction($"[Done]", true);
        }

        private void StopPyxvital()
        {
            var uninstallCmd = "taskkill";
            var uninstallArgs = "/IM Pyxvital.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            uninstallArgs = "/IM Pyxcomserveur.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
        }
    }
}
