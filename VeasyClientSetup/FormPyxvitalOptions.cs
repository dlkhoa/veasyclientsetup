﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace VeasyClientSetup
{
    public partial class formPyxvitalOptions : Form
    {
        public string PyxvitalPath { get; set; } = "Pyxvital";
        public string DataPath { get; set; }
        public string ActivationCodes { get; set; }

        public formPyxvitalOptions()
        {
            InitializeComponent();
            DialogResult = DialogResult.Cancel;
            if (!string.IsNullOrEmpty(DataPath))
            {
                textDataPath.Text = DataPath.Trim();
            }
            if (!string.IsNullOrEmpty(ActivationCodes))
            {
                textActivationCode.Text = ActivationCodes.Trim();
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            DataPath = textDataPath.Text.Trim();
            ActivationCodes = textActivationCode.Text.Trim();
            DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
