﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace VeasyClientSetup
{
    public partial class formMainPyxDriverCSD : Form
    {
        private IList<VeasyClientTool> clientTools;

        public formMainPyxDriverCSD()
        {
            InitializeComponent();
        }

        private void LogAction(string action, bool newLine = false)
        {
            textLog.AppendText(action);
            if (newLine)
            {
                textLog.AppendText("\r\n");
                textLog.Focus();
            }
        }

        private void SetupPyxvital()
        {
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var certificatePath = ConfigurationManager.AppSettings[$"vcs:CertificatePath"];
            var pyxvital = ConfigurationManager.AppSettings[$"vcs:Pyxvital"];
            var elements = pyxvital.Split(';');
            var pyxvitalPath = elements[0];
            var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, pyxvitalPath);
            var destinationPath = @"C:\ProgramData\";
            if (elements.Length > 1 && !string.IsNullOrEmpty(elements[1]))
            {
                destinationPath = elements[1];
            }
            destinationPath = Path.Combine(destinationPath, pyxvitalPath);
            LogAction($"Install PyxDriverCSD ... ");
            var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, "PyxDriver_CSD.msi");
            Utils.Execute(toolPath);
            LogAction($"[Done]", true);
            // 4 - Create shortcuts
            destinationPath = "c:\\PyxDriver";
            LogAction($"Create Pyxvital shortcuts ... ");
            //var pyxcomserveurPath = Path.Combine(destinationPath, "pyxcomserveur.bat");
            //var pyxvitalexePath = Path.Combine(destinationPath, "Pyxvital.exe");
            var pyxvitalbatPath = Path.Combine(destinationPath, "Pyxvital.bat");
            //Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath);
            Utils.CreateShortcut("Pyxvital", pyxvitalbatPath);
            //Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath, "Startup");
            Utils.CreateShortcut("Pyxvital", pyxvitalbatPath, "Startup");
            LogAction($"[Done]", true);

            // 5 - Install certificates
            LogAction($"Install Pyxvital certificates ... ");
            StopPyxvital();
            //Utils.InstallCertificate(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, certificatePath, "rootS.der"));
            //Utils.InstallCertificate(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, certificatePath, "server.der"));
            var batPath = Path.Combine(destinationPath, "rootS.bat");
            Utils.Execute(batPath);
            LogAction($"[Done]", true);

            // 6 - Start Pyxvital tools
            LogAction($"Start Pyxvital servers ... ");
            //Utils.Execute(pyxcomserveurPath, workingDir: destinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
            Utils.Execute(pyxvitalbatPath, workingDir: destinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
            LogAction($"[Done]", true);
        }

        private void SetupVDImaging()
        {
            LogAction($"Install VDImaging ... ");
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var vdImaging = ConfigurationManager.AppSettings[$"vcs:VDImaging"];
            var vdImagingPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, vdImaging);
            Utils.Execute(vdImagingPath);
            LogAction($"[Done]", true);
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Uninstall();
            SetupPyxvital();
            //SetupTools();
            SetupVDImaging();
            MessageBox.Show("Done installing components!");
            this.Close();
        }

        private void buttonUninstall_Click(object sender, EventArgs e)
        {
            Uninstall();
        }

        private void Uninstall()
        {
            UninstallPyxvital();
            //UninstallTools();
            //UninstallVDImaging();
            //MessageBox.Show("Done uninstalling components!");
            //this.Close();
        }

        private void UninstallPyxvital()
        {
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var certificatePath = ConfigurationManager.AppSettings[$"vcs:CertificatePath"];
            var pyxvital = ConfigurationManager.AppSettings[$"vcs:Pyxvital"];
            var elements = pyxvital.Split(';');
            var pyxvitalPath = elements[0];
            var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, pyxvitalPath);
            var destinationPath = @"C:\ProgramData\";
            if (elements.Length > 1 && !string.IsNullOrEmpty(elements[1]))
            {
                destinationPath = elements[1];
            }
            destinationPath = Path.Combine(destinationPath, pyxvitalPath);
            StopPyxvital();
            LogAction($"Uninstall PyxDriver ... ");
            var uninstallCmd = "wmic";
            var uninstallArgs = "product where \"name like 'PyxDriver%%'\" call uninstall";
            //var uninstallCmd = "msiexec";
            //var uninstallArgs = "/x {BDA642B2-3D00-4B6A-A583-8419FFDFCEEA} /qn";
            //Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            //uninstallArgs = "/x {A7A80AA7-7032-4C4A-BF99-E0D20A02983D} /qn";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            LogAction($"[Done]", true);
            //Delete shortcuts
            destinationPath = "c:\\PyxDriver";
            LogAction($"Delete Pyxvital shortcuts ... ");
            var pyxcomserveurPath = Path.Combine(destinationPath, "pyxcomserveur.bat");
            var pyxvitalexePath = Path.Combine(destinationPath, "Pyxvital.exe");
            Utils.DeleteShortcut("PyxcomServeur", pyxcomserveurPath);
            Utils.DeleteShortcut("Pyxvital", pyxvitalexePath);
            Utils.DeleteShortcut("PyxcomServeur", pyxcomserveurPath, "Startup");
            Utils.DeleteShortcut("Pyxvital", pyxvitalexePath, "Startup");
            //Delete Pyvital folder
            LogAction($"Delete Pyxvital from {destinationPath} ... ");
            Utils.DeleteDirectory(destinationPath);
            LogAction($"[Done]", true);
        }

        private void UninstallVDImaging()
        {
            LogAction($"Uninstall VDImaging ... ");
            var uninstallCmd = $"wmic";
            var uninstallArgs = $"product where \"name like 'VDImaging%%'\" call uninstall";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            LogAction($"[Done]", true);
        }

        private void StopPyxvital()
        {
            foreach (System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcesses())
            {
                if (myProc.ProcessName.ToLower().StartsWith("pyxdriver") ||
                    myProc.ProcessName.ToLower().StartsWith("pyxvital") ||
                    myProc.ProcessName.ToLower().StartsWith("progiciel du professionel de sant") ||
                    myProc.ProcessName.ToLower().StartsWith("pyxcomserveur"))
                {
                    myProc.Kill();
                }
            }
            /*var uninstallCmd = "taskkill";
            var uninstallArgs = "/IM PyxDriver.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            uninstallArgs = "/IM Pyxcomserveur.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);*/
        }
    }
}
