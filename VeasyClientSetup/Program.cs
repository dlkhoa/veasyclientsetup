﻿using System;
using System.Reflection;
using System.Resources;
using System.Windows.Forms;

namespace VeasyClientSetup
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, args) =>
            {
                var requestedAssemblyName = new AssemblyName(args.Name).Name;
                var manifestResourceName = new AssemblyName(Assembly.GetExecutingAssembly().FullName).Name + ".Properties.Resources.resources";
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(manifestResourceName))
                {
                    using (var reader = new ResourceReader(stream))
                    {
                        var resource = reader.GetEnumerator();
                        while (resource.MoveNext())
                        {
                            if ((string)resource.Key == requestedAssemblyName)
                                return Assembly.Load((byte[])resource.Value);
                        }
                    }
                }
                return null;
            };

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
#if _PYXDRIVER
            Application.Run(new formMainPyxDriver());
#else
#if _PYXDRIVERCSD
            Application.Run(new formMainPyxDriverCSD());
#else
#if _CPAM
            var result = MessageBox.Show("Installation des composants VEASY", "Veasy Client Setup", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                Application.Run(new formMainCPAM());
            }
        #else
            Application.Run(new formMain());
        #endif
    #endif
#endif
        }
    }
}
