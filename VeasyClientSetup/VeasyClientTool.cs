﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VeasyClientSetup
{
    class VeasyClientTool
    {
        public string Name { get; set; } = string.Empty;
        public string FileName { get; set; } = string.Empty;
        public string OSType { get; set; } = string.Empty;

        public VeasyClientTool(string toolLine)
        {
            var elements = toolLine.Split(';');
            Name = elements[0];
            if (elements.Length > 1 && !string.IsNullOrEmpty(elements[1]))
            {
                FileName = elements[1];
            }
            if (elements.Length > 2 && !string.IsNullOrEmpty(elements[2]))
            {
                OSType = elements[2];
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
