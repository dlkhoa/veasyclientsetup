﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace VeasyClientSetup
{
    public partial class formMainCPAM : Form
    {
        private Timer timerInstall = new Timer();
        private string PyxvitalPath;
        private string DataPath;
        private string ActivationCodes;
        private string DestinationPath;

        public formMainCPAM()
        {
            InitializeComponent();

            Init();
        }

        private void Init()
        {
            this.Width = 0;
            this.Height = 0;
            timerInstall.Interval = 1000; // 1 second
            timerInstall.Tick += new EventHandler(timerInstall_Tick);
            timerInstall.Start();
        }

        private void timerInstall_Tick(object sender, EventArgs e)
        {
            timerInstall.Stop();
            Install();
        }

        private void LogAction(string action, bool newLine = false)
        {
            textLog.AppendText(action);
            if (newLine)
            {
                textLog.AppendText("\r\n");
                textLog.Focus();
            }
        }

        private void SetupPyxvital()
        {
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var certificatePath = ConfigurationManager.AppSettings[$"vcs:CertificatePath"];
            var sourcePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, PyxvitalPath);
            // 1 - Copy Pyvital folder
            LogAction($"Copier Pyxvital de {sourcePath} à {DestinationPath} ... ");
            Utils.DirectoryCopy(sourcePath, DestinationPath, true);
            LogAction($"[Fin]", true);

            // 2 - Update INI file
            LogAction($"Mettre à jour fichier Pyxvital INI ... ");
            var pyxvitalDataPath = DataPath;
            if (!string.IsNullOrEmpty(pyxvitalDataPath))
            {
                var pyxvitalIni = new IniFile(Path.Combine(DestinationPath, "Pyxvital.ini"));
                pyxvitalIni.Write("Fse", Path.Combine(pyxvitalDataPath, Path.Combine("FSE", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "FSE"));
                pyxvitalIni.Write("Lots", Path.Combine(pyxvitalDataPath, Path.Combine("LOTS", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "LOTS"));
                pyxvitalIni.Write("Fichiers", Path.Combine(pyxvitalDataPath, Path.Combine("B2", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "B2"));
                pyxvitalIni.Write("Arl", Path.Combine(pyxvitalDataPath, Path.Combine("ARL", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "ARL"));
                pyxvitalIni.Write("PJ", Path.Combine(pyxvitalDataPath, Path.Combine("PJ", "#")), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "PJ"));
                pyxvitalIni.Write("PS", Path.Combine(pyxvitalDataPath, "PS"), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "PS"));
                pyxvitalIni.Write("CV", Path.Combine(pyxvitalDataPath, "CV"), "Répertoires");
                Directory.CreateDirectory(Path.Combine(pyxvitalDataPath, "CV"));
            }
            LogAction($"[Fin]", true);

            // 3 - Update Activation Code file
            LogAction($"Mettre à jour fichier Code d'Activation Pyxvital ... ");
            try
            {
                if (!string.IsNullOrEmpty(ActivationCodes))
                {
                    var codeFilePath = Path.Combine(DestinationPath, "CodPS.par");
                    var append = File.Exists(codeFilePath);
                    using (StreamWriter file = new StreamWriter(codeFilePath, append))
                    {
                        if (!append)
                        {
                            file.WriteLine("[PS]");
                        }
                        else
                        {
                            file.WriteLine("");
                        }
                        file.WriteLine(ActivationCodes);
                    }
                }
            }
            catch { }
            LogAction($"[Fin]", true);
            // 4 - Create shortcuts
            LogAction($"Créer des raccourcis vers Pyxvital ... ");
            var pyxcomserveurPath = Path.Combine(DestinationPath, "pyxcomserveur.bat");
            var pyxvitalexePath = Path.Combine(DestinationPath, "Pyxvital.exe");
            Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath);
            Utils.CreateShortcut("Pyxvital", pyxvitalexePath);
            Utils.CreateShortcut("PyxcomServeur", pyxcomserveurPath, "AllUsersStartup");
            Utils.CreateShortcut("Pyxvital", pyxvitalexePath, "AllUsersStartup");
            LogAction($"[Fin]", true);

            // 5 - Install certificates
            LogAction($"Installer des certificats Pyxvital ... ");
            StopPyxvital();
            //Utils.InstallCertificate(Path.Combine(destinationPath, "rootS.der"));
            //Utils.InstallCertificate(Path.Combine(destinationPath, "server.der"));
            var batPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, certificatePath, "rootS.bat");
            Utils.Execute(batPath);
            LogAction($"[Fin]", true);

            // 6 - Start Pyxvital tools
            var startPyxvital = true;
            if (startPyxvital)
            {
                LogAction($"Démarer des serveurs Pyxvital ... ");
                Utils.Execute(pyxcomserveurPath, workingDir: DestinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
                Utils.Execute(pyxvitalexePath, workingDir: DestinationPath, waitForExit: false, windowStyle: ProcessWindowStyle.Minimized);
                LogAction($"[Fin]", true);
            }
        }

        private void SetupVDImaging()
        {
            LogAction($"Installer VDImaging ... ");
            var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
            var vdImaging = ConfigurationManager.AppSettings[$"vcs:VDImaging"];
            var vdImagingPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, vdImaging);
            Utils.Execute(vdImagingPath);
            LogAction($"[Fin]", true);
        }

        private void SetupTools()
        {
            var i = 1;
            var clientTools = new List<VeasyClientTool>();
            while (true)
            {
                var tool = ConfigurationManager.AppSettings[$"vcs:Tool{i}"];
                if (!string.IsNullOrEmpty(tool))
                {
                    var vct = new VeasyClientTool(tool);
                    var isAdded = true;
                    if (!string.IsNullOrEmpty(vct.OSType))
                    {
                        if (!Environment.Is64BitOperatingSystem)
                        {
                            isAdded = (vct.OSType == Constants.OS32);
                        }
                    }
                    if (isAdded)
                    {
                        clientTools.Add(vct);
                    }
                }
                i++;
                if (i >= 50) break;
            }
            foreach (var tool in clientTools)
            {
                if (tool != null)
                {
                    LogAction($"Installer d'outil {tool.Name} ... ");
                    var componentPath = ConfigurationManager.AppSettings[$"vcs:ComponentPath"];
                    var toolPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, componentPath, tool.FileName);
                    Utils.Execute(toolPath);
                    LogAction($"[Fin]", true);
                }
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Install();
        }

        private void Install()
        {
            if (GetPyxvitalOptions())
            {
                SetupTools();
                SetupVDImaging();
                SetupPyxvital();
                SetupVeasy();
                LogAction("Fin d'installation des composants!");
                OutputLogToFile();
            }
            this.Close();
        }

        private void StopPyxvital()
        {
            var uninstallCmd = "taskkill";
            var uninstallArgs = "/IM Pyxvital.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
            uninstallArgs = "/IM Pyxcomserveur.exe /F";
            Utils.Execute(uninstallCmd, arguments: uninstallArgs);
        }

        private void SetupVeasy()
        {
            var iconPath = Path.Combine(DestinationPath, "Veasy.ico");
            Utils.CreateShortcut("Veasy", "https://patient.visiodent.com", iconLocation: iconPath);
            Utils.CreateShortcut("Veasy", "https://patient.visiodent.com", "AllUsersStartup", iconLocation: iconPath);
        }

        private bool GetPyxvitalOptions()
        {
            var pyxvital = ConfigurationManager.AppSettings[$"vcs:Pyxvital"];
            var activationCodes = ConfigurationManager.AppSettings[$"vcs:ActivationCodes"];
            var dataPath = ConfigurationManager.AppSettings[$"vcs:DataPath"];
            var resume = true;
            var pyxvitalPath = "Pyxvital";
            var destinationPath = @"C:\ProgramData";
            if (!string.IsNullOrEmpty(pyxvital))
            {
                var elements = pyxvital.Split(';');
                pyxvitalPath = elements[0];
                if (elements.Length > 1 && !string.IsNullOrEmpty(elements[1]))
                {
                    destinationPath = elements[1];
                }
                destinationPath = Path.Combine(destinationPath, pyxvitalPath);
            }
            DestinationPath = destinationPath;
            if (string.IsNullOrEmpty(dataPath) || string.IsNullOrEmpty(activationCodes))
            {
                var dlg = new formPyxvitalOptions();
                dlg.DataPath = dataPath;
                dlg.ActivationCodes = activationCodes;
                var result = dlg.ShowDialog();
                if (result == DialogResult.OK)
                {
                    PyxvitalPath = dlg.PyxvitalPath;
                    DataPath = dlg.DataPath;
                    ActivationCodes = dlg.ActivationCodes;
                }
                else
                {
                    resume = false;
                }
            }
            else
            {
                PyxvitalPath = pyxvitalPath;
                DataPath = dataPath;
                ActivationCodes = activationCodes.Replace(";", "\r\n");
            }
            return resume;
        }

        private void OutputLogToFile()
        {
            var logPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"VeasyClientSetup_log_{DateTime.Now.ToString("yyyyMMddHHmmss")}.txt");
            System.IO.File.WriteAllText(logPath, textLog.Text);
        }
    }
}
